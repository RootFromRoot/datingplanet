package com.app.datingplanet.data.job

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator
import timber.log.Timber

class JobCreator : JobCreator {

    override fun create(tag: String): Job? {
        return if (CheckJob.TAG == tag) {
            Timber.i("Job created")
            CheckJob()
        } else null
    }
}