package com.app.datingplanet.data.db

import androidx.room.*

@Dao
interface SiteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(site: Site)

    @Update
    fun update(site: Site)

    @Delete
    fun delete(site: Site)

    @Query("SELECT * from siteDao")
    fun getList(): List<Site>
}