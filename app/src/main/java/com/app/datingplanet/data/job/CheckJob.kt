package com.app.datingplanet.data.job

import android.content.Context.MODE_PRIVATE
import com.app.datingplanet.data.Application
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import timber.log.Timber
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.Reader
import java.net.URL
import java.nio.charset.Charset

class CheckJob : Job() {
    private lateinit var mDatabase: DatabaseReference

    companion object {
        const val TAG = "check_job_tag"
        fun scheduleJob() {
            JobRequest.Builder(TAG)
                .setPeriodic(900000)
                .build()
                .schedule()
        }
    }

    override fun onRunJob(params: Params): Result {
        Timber.i("RunningJob")
        val sharedPreferences = Application.getContext()
            .getSharedPreferences("mypref", MODE_PRIVATE)
        val fbKey = sharedPreferences.getString("fbKey", null) ?: ""
        mDatabase = FirebaseDatabase.getInstance().getReference("users")
            .child(fbKey)
        var title: String? = null
        val url = mDatabase.child("conversion").toString() + ".json?auth=I2wyyYq4eBPdM0pZ8h3RJjG0CzhjTbfmTrYTmFdz"
        try {
            title = readJsonFromUrl2(url)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        Timber.i("Data fetched: Title: $title, Url:$url")
        if (title != null && title != "null" && title == "1") {
            Timber.i("Check passed ")
            val editor = Application.getContext()
                .getSharedPreferences("mypref", MODE_PRIVATE).edit()
            editor.putBoolean("isSendConv", true)
            editor.apply()
            editor.commit()
        }
        return Result.SUCCESS
    }

    @Throws(IOException::class)
    fun readJsonFromUrl2(url: String): String {
        val inputStream = URL(url).openStream()
        inputStream.use {
            val rd = BufferedReader(InputStreamReader(it, Charset.forName("UTF-8")))
            return readAll(rd)
        }
    }

    @Throws(IOException::class)
    private fun readAll(rd: Reader): String {
        val sb = StringBuilder()
        var cp: Int = rd.read()
        while (cp != -1) {
            cp = rd.read()
            sb.append(cp.toChar())
        }
        return sb.toString()
    }

}