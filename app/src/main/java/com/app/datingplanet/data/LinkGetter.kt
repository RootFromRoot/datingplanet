package com.app.datingplanet.data

import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import timber.log.Timber

object LinkGetter {
    fun getLinkList(isDataFetched: (map: Map<String, Any>?) -> Unit) {
        val docRef = FirebaseFirestore.getInstance().collection("links").document("url")
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val document = task.result
                if (document!!.exists()) {
                    val map = document.data
                    isDataFetched(map)
                    Timber.d("DocumentSnapshot data: %s", map!!)
                } else {
                    Timber.d("No such document")
                }
            } else {
                Timber.d(task.exception, "get failed with")
            }
        }
    }
}