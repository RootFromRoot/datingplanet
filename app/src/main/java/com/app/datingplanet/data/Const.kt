package com.app.datingplanet.data

const val RC_SITE_ADDED = 1
const val PREF_NAME_FIRST_LAUNCH = "PREF_NAME_FIRST_LAUNCH"

/* -- PERMISSION VARIABLES -- */

// enable JavaScript for webview
const val  ASWP_JSCRIPT = true

// upload file from webview
const val  ASWP_FUPLOAD = true

// enable upload from camera for photos
const val  ASWP_CAMUPLOAD = true

// incase you want only camera files to upload
const val  ASWP_ONLYCAM = false

// upload multiple files in webview
const val  ASWP_MULFILE = true

// track GPS locations
const val  ASWP_LOCATION = true

// show ratings dialog; auto configured
// edit method get_rating() for customizations
const val  ASWP_RATINGS = true

// pull refresh current url
const val  ASWP_PULLFRESH = true

// show progress bar in app
const val  ASWP_PBAR = true

// zoom control for webpages view
const val  ASWP_ZOOM = false

// save form cache and auto-fill information
const val  ASWP_SFORM = false

// whether the loading webpages are offline or online
const val  ASWP_OFFLINE = false

// open external url with default browser instead of app webview
const val  ASWP_EXTURL = false


/* -- SECURITY const val IABLES -- */

// verify whether HTTPS port needs certificate verification
const val  ASWP_CERT_VERIFICATION = true


/* -- CONFIG const val IABLES -- */

//complete URL of your website or offline webpage
const val  ASWV_URL = "https://transfiles.ru"

//to upload any file type using "*/*"; check file type references for more
const val  ASWV_F_TYPE = "*/*"


/* -- RATING SYSTEM const val IABLES -- */

const val  ASWR_DAYS = 3    // after how many days of usage would you like to show the dialoge
const val  ASWR_TIMES = 10  // overall request launch times being ignored
const val  ASWR_INTERVAL = 2   // reminding users to rate after days interval

const val  asw_file_req = 1