package com.app.datingplanet.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "siteDao")
data class Site(

    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0,

    var name: String,
    var link: String,
    var isFavorite: Boolean
)