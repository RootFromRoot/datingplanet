package com.app.datingplanet.data

import android.annotation.SuppressLint
import android.content.Context
import androidx.multidex.MultiDexApplication
import androidx.room.Room
import com.app.datingplanet.data.db.AppDatabase
import com.app.datingplanet.data.job.CheckJob
import com.app.datingplanet.data.job.JobCreator
import com.evernote.android.job.JobManager

class Application: MultiDexApplication() {
    lateinit var db: AppDatabase

    override fun onCreate() {
        super.onCreate()
        context = this
        initRoom()
        JobManager.create(this).addJobCreator(JobCreator())
        CheckJob.scheduleJob()
    }

    private fun initRoom() {
        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "sites.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private lateinit var context: Context
        fun getContext() = context
    }
}