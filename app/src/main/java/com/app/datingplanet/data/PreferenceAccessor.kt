package com.app.datingplanet.data

import android.content.Context
import android.content.SharedPreferences

object PreferenceAccessor {
    fun saveBooleanToPref(context: Context, name: String, value: Boolean) {
        val sharedPref =
            context.getSharedPreferences(PREF_NAME_FIRST_LAUNCH, Context.MODE_PRIVATE)
        with(sharedPref.edit()) {
            putBoolean(name, value)
            apply()
        }
    }

    fun getBooleanFromPref(context: Context, name: String): Boolean {
        val sharedPref =
            context.getSharedPreferences(PREF_NAME_FIRST_LAUNCH, Context.MODE_PRIVATE)
        return sharedPref.getBoolean(name, true)
    }
}