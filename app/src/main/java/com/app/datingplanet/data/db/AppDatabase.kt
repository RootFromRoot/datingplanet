package com.app.datingplanet.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.datingplanet.data.db.Site
import com.app.datingplanet.data.db.SiteDao

@Database(entities = [Site::class], version = 1)

abstract class AppDatabase : RoomDatabase() {
    abstract fun siteDao(): SiteDao
}