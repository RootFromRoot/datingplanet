package com.app.datingplanet.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.datingplanet.R
import com.app.datingplanet.data.db.Site
import kotlinx.android.synthetic.main.list_item_site.view.*

class SiteListAdapter : RecyclerView.Adapter<SiteHolder>() {

    private var items: ArrayList<Site> = ArrayList()
    var onItemClick:(site: Site)->Unit = {}
    var onStarBtnClick:(site: Site, position: Int)->Unit = {_,_->}
    var onRemoveBtnClick:(site: Site)->Unit = {}

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SiteHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_site, parent, false))

    fun setItems(items: ArrayList<Site>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun addItem(site: Site) {
        items.add(site)
        notifyItemInserted(itemCount - 1)
    }

    fun removeItem(site: Site, position: Int) {
        items.remove(site)
        notifyItemRemoved(position)
    }

    fun changeItem(site: Site, position: Int) {
        items[position] = site
        notifyItemChanged(position)
    }

    override fun onBindViewHolder(holder: SiteHolder, position: Int) {
        holder.view.apply {
            if (items[position].isFavorite) ic_star.setImageResource(R.drawable.ic_star_black_24dp)
            else ic_star.visibility = View.INVISIBLE
            tv_title.text = items[position].name
            btn_remove.setOnClickListener { onRemoveBtnClick(items[position]) }
            ic_star.setOnClickListener { onStarBtnClick(items[position], position) }
            if (items[position].isFavorite) ic_star.visibility = View.VISIBLE
            this.setOnClickListener { onItemClick(items[position]) }
        }
    }
}

class SiteHolder(val view: View) : RecyclerView.ViewHolder(view)