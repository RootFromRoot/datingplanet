package com.app.datingplanet.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.datingplanet.R
import com.app.datingplanet.data.Application
import com.app.datingplanet.data.db.Site
import com.app.datingplanet.data.db.SiteDao
import kotlinx.android.synthetic.main.activity_setup_site.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.URL

class SetupSiteActivity : AppCompatActivity() {

    private lateinit var dao: SiteDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup_site)

        dao = (applicationContext as Application).db.siteDao()

        iv_back.setOnClickListener { onBackPressed() }

        btn_add.setOnClickListener {
            if (et_site.text.toString().trim().isNotEmpty()) {
                if (et_site.text.toString().contains("http://") || et_site.text.toString().contains("https://")) {
                    GlobalScope.launch {
                        dao.insert(Site(null, URL(et_site.text.toString()).host, et_site.text.toString(), false))
                    }
                } else {
                    GlobalScope.launch {
                        val url = try {
                            URL(et_site.text.toString()).host
                        } catch (e: Exception) {
                            URL("https://" + et_site.text.toString()).host
                        }
                        dao.insert(
                            Site(
                                id = null,
                                name = url,
                                link = et_site.text.toString(),
                                isFavorite = false
                            )
                        )
                    }
                }

                onBackPressed()
            } else Toast.makeText(this, "Add site your site", Toast.LENGTH_SHORT).show()
        }
    }
}
