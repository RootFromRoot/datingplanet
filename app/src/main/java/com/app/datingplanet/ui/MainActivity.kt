package com.app.datingplanet.ui

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.datingplanet.R
import com.app.datingplanet.data.ASWP_MULFILE
import com.app.datingplanet.data.Application
import com.app.datingplanet.data.RC_SITE_ADDED
import com.app.datingplanet.data.asw_file_req
import com.app.datingplanet.data.db.Site
import com.app.datingplanet.data.db.SiteDao
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber


class MainActivity : AppCompatActivity() {

    private val adapter = SiteListAdapter()
    private lateinit var dao: SiteDao
    private var siteList: ArrayList<Site> = ArrayList()
    private var selectedSite: Site = Site(0, "", "", true)
    private lateinit var webViewConfigurator: WebViewConfigurator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        wrapper_setup_sites.setOnClickListener {
            startActivityForResult(Intent(this, SetupSiteActivity::class.java), RC_SITE_ADDED)
        }
        setSupportActionBar(toolbar)
        setupDrawer()

        dao = (applicationContext as Application).db.siteDao()
        webViewConfigurator = WebViewConfigurator(this, wv)
        setupWebView()

        getList { list ->
            list.forEach { if (it.isFavorite) showWebView(it) }
            adapter.setItems(list)
            Timber.i("List: $list")
        }
        chb_star.isChecked = selectedSite.isFavorite

        chb_star.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) markSiteInList(selectedSite)
        }
    }

    private fun setupWebView() {
        webViewConfigurator.checkPermissions()
        webViewConfigurator.atachOnWebViewClickListener(wv)
        webViewConfigurator.atachOnWebViewDownloadListener(wv)
    }

    private fun getList(isFetched: (list: ArrayList<Site>) -> Unit) {
        GlobalScope.launch {
            siteList = dao.getList() as ArrayList<Site>
            filterList()
            withContext(Dispatchers.Main) { isFetched(siteList) }
        }
    }

    private fun filterList() {
        var isContainFavorite = false

        siteList.forEachIndexed { index, site ->
            val buff: Site = siteList[0]
            if (site.isFavorite) {
                siteList[index] = buff
                siteList[0] = site
                isContainFavorite = true
            }
        }
        if (!isContainFavorite) {
            siteList[0].isFavorite = true
            GlobalScope.launch { dao.insert(siteList[0]) }
        }
    }

    private fun setupDrawer() {
        setupRecyclerView()

        val toggle = ActionBarDrawerToggle(
            this,
            drawer_layout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun setupRecyclerView() {
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = adapter

        adapter.onRemoveBtnClick = { site ->
            showConfirmPopup {
                removeSiteFromDb(site) { getList { adapter.setItems(it) } }
            }
        }
        adapter.onStarBtnClick = { site, i ->
            site.isFavorite = true
            markSiteInList(site)
            adapter.setItems(siteList)
        }
        adapter.onItemClick = {
            selectedSite = it
            showWebView(it)
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    private fun markSiteInList(site: Site) {
        siteList.forEach {
            it.isFavorite = false
            if (it == site) it.isFavorite = true
            GlobalScope.launch { dao.insert(it) }
        }
        filterList()
        adapter.setItems(siteList)
    }

    private fun showConfirmPopup(onPositiveClick: () -> Unit) {
        AlertDialog.Builder(this)
            .setMessage("Do you really want to delete?")
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(android.R.string.yes) { dialog, _ ->
                onPositiveClick()
                dialog.dismiss()
            }
            .setNegativeButton(android.R.string.no, null).show()
    }

    private fun removeSiteFromDb(site: Site, isDeleted: () -> Unit) {
        GlobalScope.launch {
            dao.delete(site)
            isDeleted()
        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
        }
    }

    private fun showWebView(site: Site) {
        //FinestWebView.Builder(this).titleDefault(title).show(url)
        wv.loadUrl(site.link)
        tlb_title.text = site.name
        Timber.i("Site: ${site.name}")
        chb_star.isChecked = site.isFavorite
        adapter.setItems(siteList)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == RC_SITE_ADDED) getList { adapter.setItems(it) }
        else {
            try {
                java.util.concurrent.TimeUnit.SECONDS.sleep(4)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            if (Build.VERSION.SDK_INT >= 21) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = resources.getColor(R.color.colorPrimary)
                var results: Array<Uri> = arrayOf(Uri.EMPTY)
                if (resultCode == Activity.RESULT_OK) {
                    if (requestCode == asw_file_req) {
                        if (null == webViewConfigurator.asw_file_path) {
                            return
                        }
                        if (intent == null || intent.data == null) {
                            if (webViewConfigurator.asw_cam_message != null) {
                                results = arrayOf(Uri.parse(webViewConfigurator.asw_cam_message))
                            }
                        } else {
                            val dataString = intent.dataString
                            if (dataString != null) {
                                results = arrayOf(Uri.parse(dataString))
                            } else {
                                if (ASWP_MULFILE) {
                                    if (intent.clipData != null) {
                                        val numSelectedFiles: Int = intent.clipData!!.itemCount
                                        results = arrayOf(Uri.EMPTY)
                                        for (i in 0 until numSelectedFiles) {
                                            results[i] = intent.clipData!!.getItemAt(i).uri
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                webViewConfigurator.asw_file_path.onReceiveValue(results)
                webViewConfigurator.asw_file_path = null
            } else {
                if (requestCode == asw_file_req) {
                    if (null == webViewConfigurator.asw_file_message) return
                    val result = if (intent == null || resultCode != RESULT_OK) null else intent.data
                    webViewConfigurator.asw_file_message.onReceiveValue(result)
                    webViewConfigurator.asw_file_message = null
                }
            }
        }
    }

}
