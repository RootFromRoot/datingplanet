package com.app.datingplanet.ui

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.app.datingplanet.R
import com.app.datingplanet.data.Application
import com.app.datingplanet.data.LinkGetter
import com.app.datingplanet.data.PREF_NAME_FIRST_LAUNCH
import com.app.datingplanet.data.PreferenceAccessor
import com.app.datingplanet.data.db.Site
import com.app.datingplanet.data.db.SiteDao
import kotlinx.android.synthetic.main.activity_logo.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LogoActivity : AppCompatActivity() {

    private lateinit var dao: SiteDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_logo)
        dao = (applicationContext as Application).db.siteDao()
        if (checkIsFirstLaunch()) initLocalDb()
        if (!checkIsFirstLaunch())startActivity(Intent(this, MainActivity::class.java))
        btn_next.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
    }

    private fun checkIsFirstLaunch(): Boolean {
        return PreferenceAccessor.getBooleanFromPref(this, PREF_NAME_FIRST_LAUNCH)
    }

    private fun initLocalDb() {
        LinkGetter.getLinkList { map ->
            val linkList: ArrayList<String> = ArrayList()
            linkList.add(map!!["url1"].toString())
            linkList.add(map["url2"].toString())
            linkList.add(map["url3"].toString())
            linkList.add(map["url4"].toString())
            linkList.add(map["url5"].toString())
            linkList.add(map["url6"].toString())
            linkList.forEachIndexed { i, link ->
                addLinkToDB("Site ${i + 1}", link, i == 0)
            }
        }
    }

    private fun addLinkToDB(siteName: String, link: String, isFav: Boolean) {
        GlobalScope.launch {
            dao.insert(Site(null, siteName, link, isFav))
        }
        saveFirstLaunchPref()
    }

    private fun saveFirstLaunchPref() {
        PreferenceAccessor.saveBooleanToPref(this, PREF_NAME_FIRST_LAUNCH, false)
    }
}
